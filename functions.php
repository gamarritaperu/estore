<?php
add_action( 'after_setup_theme', 'et_setup_theme' );
if ( ! function_exists( 'et_setup_theme' ) ){
	function et_setup_theme(){
		global $themename, $shortname, $default_colorscheme;
		$themename = "eStore";
		$shortname = "estore";
		$default_colorscheme = 'Default';

		$template_dir = get_template_directory();

		require_once($template_dir . '/epanel/custom_functions.php');

		require_once($template_dir . '/includes/functions/sidebars.php');

		load_theme_textdomain('eStore',$template_dir.'/lang');

		require_once($template_dir . '/epanel/core_functions.php');

		require_once($template_dir . '/epanel/post_thumbnails_estore.php');

		include($template_dir . '/includes/widgets.php');

		require_once($template_dir . '/includes/functions/additional_functions.php');

		add_action( 'pre_get_posts', 'et_home_posts_query' );

		add_action( 'et_epanel_changing_options', 'et_delete_featured_ids_cache' );
		add_action( 'delete_post', 'et_delete_featured_ids_cache' );
		add_action( 'save_post', 'et_delete_featured_ids_cache' );

		add_action( 'wp_enqueue_scripts', 'et_load_scripts_styles' );

		add_theme_support( 'woocommerce' );

		add_action( 'body_class', 'et_add_woocommerce_class_to_homepage' );

		// take breadcrumbs out of .container
		remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
		// woocommerce_breadcrumb function is overwritten in functions.php
		add_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 9, 0 );
	}
}

function et_load_scripts_styles() {
	global $shortname;

	// load Raleway from Google Fonts
	$protocol = is_ssl() ? 'https' : 'http';
	$query_args = array(
		'family' => 'Raleway:400,300,200'
	);
	wp_enqueue_style( 'estore-fonts', add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" ), array(), null );

	$et_prefix = ! et_options_stored_in_one_row() ? "{$shortname}_" : '';
	$heading_font_option_name = "{$et_prefix}heading_font";
	$body_font_option_name = "{$et_prefix}body_font";

	$et_gf_enqueue_fonts = array();
	$et_gf_heading_font = sanitize_text_field( et_get_option( $heading_font_option_name, 'none' ) );
	$et_gf_body_font = sanitize_text_field( et_get_option( $body_font_option_name, 'none' ) );

	if ( 'none' != $et_gf_heading_font ) $et_gf_enqueue_fonts[] = $et_gf_heading_font;
	if ( 'none' != $et_gf_body_font ) $et_gf_enqueue_fonts[] = $et_gf_body_font;

	if ( ! empty( $et_gf_enqueue_fonts ) ) et_gf_enqueue_fonts( $et_gf_enqueue_fonts );
}

// overwrite woocommerce_breadcrumb function to change wrap_before and wrap_after arguments
function woocommerce_breadcrumb( $args = array() ) {
	$defaults = array(
		'delimiter'  => ' &rsaquo; ',
		'wrap_before'  => '<div id="breadcrumbs" itemprop="breadcrumb">',
		'wrap_after' => '</div>',
		'before'   => '',
		'after'   => '',
		'home'    => null
	);

	$args = wp_parse_args( $args, $defaults  );

	woocommerce_get_template( 'shop/breadcrumb.php', $args );
}

add_action('wp_head','et_portfoliopt_additional_styles',100);
function et_portfoliopt_additional_styles(){ ?>
	<style type="text/css">
		#et_pt_portfolio_gallery { margin-left: -41px; }
		.et_pt_portfolio_item { margin-left: 31px; }
		.et_portfolio_small { margin-left: -40px !important; }
		.et_portfolio_small .et_pt_portfolio_item { margin-left: 29px !important; }
		.et_portfolio_large { margin-left: -24px !important; }
		.et_portfolio_large .et_pt_portfolio_item { margin-left: 4px !important; }
	</style>
<?php }

function register_main_menus() {
	register_nav_menus(
		array(
			'primary-menu' => __( 'Primary Menu' ),
			'secondary-menu' => __( 'Secondary Menu' )
		)
	);
};
if (function_exists('register_nav_menus')) add_action( 'init', 'register_main_menus' );

/**
 * Gets featured posts IDs from transient, if the transient doesn't exist - runs the query and stores IDs
 */
function et_get_featured_posts_ids(){
	if ( false === ( $et_featured_post_ids = get_transient( 'et_featured_post_ids' ) ) ) {
		if ( class_exists( 'woocommerce' ) ) {
			// show WooCommerce products from taxonomy term set in ePanel
			$featured_cat = get_option('estore_feat_cat');
			$featured_num = get_option('estore_featured_num');
			$et_featured_term = get_term_by( 'name', $featured_cat, 'product_cat' );

			$featured_query = new WP_Query(
				apply_filters( 'et_woocommerce_featured_args',
					array(
						'post_type' => 'product',
						'posts_per_page' => (int) $featured_num,
						'meta_query' => array(
							array( 'key' => '_visibility', 'value' => array( 'catalog', 'visible' ),'compare' => 'IN' )
						),
						'tax_query' => array(
							array(
								'taxonomy' 	=> 'product_cat',
								'field' 	=> 'id',
								'terms' 	=> (int) $et_featured_term->term_id
							)
						)
					)
				)
			);
		} else {
			// grab normal posts from selected category
			$featured_query = new WP_Query( apply_filters( 'et_featured_post_args', array(
				'posts_per_page'	=> (int) et_get_option( 'estore_featured_num' ),
				'cat'				=> (int) get_catId( et_get_option( 'estore_feat_cat' ) )
			) ) );
		}

		if ( $featured_query->have_posts() ) {
			while ( $featured_query->have_posts() ) {
				$featured_query->the_post();

				$et_featured_post_ids[] = get_the_ID();
			}

			set_transient( 'et_featured_post_ids', $et_featured_post_ids );
		}

		wp_reset_postdata();
	}

	return $et_featured_post_ids;
}

/**
 * Filters the main query on homepage
 */
function et_home_posts_query( $query = false ) {
	/* Don't proceed if it's not homepage or the main query */
	if ( ! is_home() || ! is_a( $query, 'WP_Query' ) || ! $query->is_main_query() ) return;

	/* Set the amount of posts per page on homepage */
	$query->set( 'posts_per_page', et_get_option( 'estore_homepage_posts', '6' ) );

	/* Exclude slider posts, if the slider is activated, pages are not featured and posts duplication is disabled in ePanel  */
	if ( 'on' == et_get_option( 'estore_featured', 'on' ) && 'false' == et_get_option( 'estore_use_pages', 'false' ) && 'false' == et_get_option( 'estore_duplicate', 'on' ) )
		$query->set( 'post__not_in', et_get_featured_posts_ids() );

	/* Exclude categories set in ePanel */
	$exclude_categories = et_get_option( 'estore_exlcats_recent', false );

	if ( ! class_exists( 'woocommerce' ) ) {
		if ( $exclude_categories ) $query->set( 'category__not_in', array_map( 'intval', et_generate_wpml_ids( $exclude_categories, 'category' ) ) );
	} else {
		/* Display WooCommerce products on homepage */
		$query->set( 'post_type', 'product' );
		$query->set( 'meta_query', array(
				array( 'key' => '_visibility', 'value' => array( 'catalog', 'visible' ),'compare' => 'IN' )
			)
		);

		if ( $exclude_categories ) {
			$query->set( 'tax_query', array(
					array(
						'taxonomy' 	=> 'product_cat',
						'field' 	=> 'id',
						'operator'	=> 'NOT IN',
						'terms'		=> (array) $exclude_categories
					)
				)
			);
		}
	}
}

/**
 * Deletes featured posts IDs transient, when the user saves, resets ePanel settings, creates or moves posts to trash in WP-Admin
 */
function et_delete_featured_ids_cache(){
	if ( false !== get_transient( 'et_featured_post_ids' ) ) delete_transient( 'et_featured_post_ids' );
}

if ( ! function_exists( 'et_list_pings' ) ){
	function et_list_pings($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment; ?>
		<li id="comment-<?php comment_ID(); ?>"><?php comment_author_link(); ?> - <?php comment_excerpt(); ?>
	<?php }
}

function et_epanel_custom_colors_css(){
	global $shortname; ?>

	<style type="text/css">
		body { color: #<?php echo esc_html(get_option($shortname.'_color_mainfont')); ?>; }
		body { background-color: #<?php echo esc_html(get_option($shortname.'_color_bgcolor')); ?>; }
		.post a:link, .post a:visited { color: #<?php echo esc_html(get_option($shortname.'_color_mainlink')); ?>; }
		ul.nav li a { color: #<?php echo esc_html(get_option($shortname.'_color_pagelink')); ?>; }
		#sidebar h3 { color:#<?php echo esc_html(get_option($shortname.'_color_sidebar_titles')); ?>; }
		#footer, p#copyright { color:#<?php echo esc_html(get_option($shortname.'_color_footer')); ?> !important; }
		#footer a { color:#<?php echo esc_html(get_option($shortname.'_color_footer_links')); ?> !important; }
	</style>
<?php
}

function et_add_woocommerce_class_to_homepage( $classes ) {
	if ( is_home() ) $classes[] = 'woocommerce';

	return $classes;
}

if ( function_exists( 'get_custom_header' ) ) {
	// compatibility with versions of WordPress prior to 3.4

	add_action( 'customize_register', 'et_estore_customize_register' );
	function et_estore_customize_register( $wp_customize ) {
		$google_fonts = et_get_google_fonts();

		$font_choices = array();
		$font_choices['none'] = 'Default Theme Font';
		foreach ( $google_fonts as $google_font_name => $google_font_properties ) {
			$font_choices[ $google_font_name ] = $google_font_name;
		}

		$wp_customize->remove_section( 'title_tagline' );
		$wp_customize->remove_section( 'background_image' );
		$wp_customize->remove_section( 'colors' );

		$wp_customize->add_section( 'et_google_fonts' , array(
			'title'		=> __( 'Fonts', 'eStore' ),
			'priority'	=> 50,
		) );

		$wp_customize->add_setting( 'estore_heading_font', array(
			'default'		=> 'none',
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options'
		) );

		$wp_customize->add_control( 'estore_heading_font', array(
			'label'		=> __( 'Header Font', 'eStore' ),
			'section'	=> 'et_google_fonts',
			'settings'	=> 'estore_heading_font',
			'type'		=> 'select',
			'choices'	=> $font_choices
		) );

		$wp_customize->add_setting( 'estore_body_font', array(
			'default'		=> 'none',
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options'
		) );

		$wp_customize->add_control( 'estore_body_font', array(
			'label'		=> __( 'Body Font', 'eStore' ),
			'section'	=> 'et_google_fonts',
			'settings'	=> 'estore_body_font',
			'type'		=> 'select',
			'choices'	=> $font_choices
		) );
	}

	add_action( 'wp_head', 'et_estore_add_customizer_css' );
	add_action( 'customize_controls_print_styles', 'et_estore_add_customizer_css' );
	function et_estore_add_customizer_css(){ ?>
		<style type="text/css">
		<?php
			global $shortname;

			$et_prefix = ! et_options_stored_in_one_row() ? "{$shortname}_" : '';
			$heading_font_option_name = "{$et_prefix}heading_font";
			$body_font_option_name = "{$et_prefix}body_font";

			$et_gf_heading_font = sanitize_text_field( et_get_option( $heading_font_option_name, 'none' ) );
			$et_gf_body_font = sanitize_text_field( et_get_option( $body_font_option_name, 'none' ) );

			if ( 'none' != $et_gf_heading_font || 'none' != $et_gf_body_font ) :

				if ( 'none' != $et_gf_heading_font )
					et_gf_attach_font( $et_gf_heading_font, 'h1, h2, h3, h4, h5, h6, .description h2.title, .item-content h4, .product h3, .post h1, .post h2, .post h3, .post h4, .post h5, .post h6, .related-items span, .page-title, .product_title' );

				if ( 'none' != $et_gf_body_font )
					et_gf_attach_font( $et_gf_body_font, 'body' );

			endif;
		?>
		</style>
	<?php }

	add_action( 'customize_controls_print_footer_scripts', 'et_load_google_fonts_scripts' );
	function et_load_google_fonts_scripts() {
		wp_enqueue_script( 'et_google_fonts', get_template_directory_uri() . '/epanel/google-fonts/et_google_fonts.js', array( 'jquery' ), '1.0', true );
		wp_localize_script( 'et_google_fonts', 'et_google_fonts', array(
			'options_in_one_row' => ( et_options_stored_in_one_row() ? 1 : 0 )
		) );
	}

	add_action( 'customize_controls_print_styles', 'et_load_google_fonts_styles' );
	function et_load_google_fonts_styles() {
		wp_enqueue_style( 'et_google_fonts_style', get_template_directory_uri() . '/epanel/google-fonts/et_google_fonts.css', array(), null );
	}

	function my_custom_post_tienda() {
	$labels = array(
		'name'               => _x( 'Tiendas', 'post type general name' ),
		'singular_name'      => _x( 'Tienda', 'post type singular name' ),
		'add_new'            => _x( 'Agregar nueva tienda', 'book' ),
		'add_new_item'       => __( 'Agregar nueva tienda' ),
		'edit_item'          => __( 'Editar tienda' ),
		'new_item'           => __( 'Nueva tienda' ),
		'all_items'          => __( 'Todas las tiendas' ),
		'view_item'          => __( 'Ver tienda' ),
		'search_items'       => __( 'Buscar tiendas' ),
		'not_found'          => __( 'No se encontraron tiendas' ),
		'not_found_in_trash' => __( 'No se encontraron tiendas en la papelera' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Tiendas'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Holds our products and product specific data',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'   => true,
		'capability_type'=> 'post',
	);
	register_post_type( 'tienda', $args );	
	}
	
	add_action( 'init', 'my_custom_post_tienda' );

	function my_custom_post_aviso() {
	$labels = array(
		'name'               => _x( 'Avisos', 'post type general name' ),
		'singular_name'      => _x( 'Aviso', 'post type singular name' ),
		'add_new'            => _x( 'Agregar nuevo aviso', 'book' ),
		'add_new_item'       => __( 'Agregar nuevo aviso' ),
		'edit_item'          => __( 'Editar aviso' ),
		'new_item'           => __( 'Nuevo aviso' ),
		'all_items'          => __( 'Todas los avisos' ),
		'view_item'          => __( 'Ver aviso' ),
		'search_items'       => __( 'Buscar avisos' ),
		'not_found'          => __( 'No se encontraron avisos' ),
		'not_found_in_trash' => __( 'No se encontraron avisos en la papelera' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Avisos'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Holds our products and product specific data',
		'public'        => true,
		'menu_position' => 6,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'comments' ),
		'has_archive'   => true,
		'capability_type'=> 'post',
	);
	register_post_type( 'aviso', $args );	
	}
	
	add_action( 'init', 'my_custom_post_aviso' );

	function my_custom_post_galeria() {
	$labels = array(
		'name'               => _x( 'Galerias', 'post type general name' ),
		'singular_name'      => _x( 'Galeria', 'post type singular name' ),
		'add_new'            => _x( 'Agregar nueva galería', 'book' ),
		'add_new_item'       => __( 'Agregar nueva galería' ),
		'edit_item'          => __( 'Editar galería' ),
		'new_item'           => __( 'Nueva galería' ),
		'all_items'          => __( 'Todas las galerías' ),
		'view_item'          => __( 'Ver galería' ),
		'search_items'       => __( 'Buscar galerías' ),
		'not_found'          => __( 'No se encontraron galerías' ),
		'not_found_in_trash' => __( 'No se encontraron galerías en la papelera' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Galerías'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Holds our products and product specific data',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'   => true,
		'capability_type'=> 'post',
	);
	register_post_type( 'galeria', $args );	
	}

	add_action( 'init', 'my_custom_post_galeria' );

	/*******************************************************************
	* @Author: Boutros AbiChedid
	* @Date:   March 20, 2011
	* @Websites: http://bacsoftwareconsulting.com/
	* http://blueoliveonline.com/
	* @Description: Numbered Page Navigation (Pagination) Code.
	* @Tested: Up to WordPress version 3.1.2 (also works on WP 3.3.1)
	********************************************************************/
	 
	/* Function that Rounds To The Nearest Value.
	   Needed for the pagenavi() function */ 
	function round_num($num, $to_nearest) {
	   /*Round fractions down (http://php.net/manual/en/function.floor.php)*/
	   return floor($num/$to_nearest)*$to_nearest;
	}
	 
	/* Function that performs a Boxed Style Numbered Pagination (also called Page Navigation).
	   Function is largely based on Version 2.4 of the WP-PageNavi plugin */  
	function pagenavi($before = '', $after = '') { 
	    global $wpdb, $wp_query;
	    $pagenavi_options = array();
	    $pagenavi_options['pages_text'] = ('Página %CURRENT_PAGE% de %TOTAL_PAGES%:');
	    $pagenavi_options['current_text'] = '%PAGE_NUMBER%';
	    $pagenavi_options['page_text'] = '%PAGE_NUMBER%';
	    $pagenavi_options['first_text'] = ('First Page');
	    $pagenavi_options['last_text'] = ('Last Page');
	    $pagenavi_options['next_text'] = 'Siguiente &raquo;';
	    $pagenavi_options['prev_text'] = '&laquo; Anterior';
	    $pagenavi_options['dotright_text'] = '...';
	    $pagenavi_options['dotleft_text'] = '...';
	    $pagenavi_options['num_pages'] = 5; //continuous block of page numbers
	    $pagenavi_options['always_show'] = 0;
	    $pagenavi_options['num_larger_page_numbers'] = 0;
	    $pagenavi_options['larger_page_numbers_multiple'] = 5;
	     
	    //If NOT a single Post is being displayed
	    /*http://codex.wordpress.org/Function_Reference/is_single)*/
	    if (!is_single()) {
	        $request = $wp_query->request;
	        //intval ‚Äö√Ñ√∂‚àö√ë‚àö√Ü Get the integer value of a variable
	        /*http://php.net/manual/en/function.intval.php*/
	        $posts_per_page = intval(get_query_var('posts_per_page'));
	        //Retrieve variable in the WP_Query class.
	        /*http://codex.wordpress.org/Function_Reference/get_query_var*/
	        $paged = intval(get_query_var('paged'));
	        $numposts = $wp_query->found_posts;
	        $max_page = $wp_query->max_num_pages;
	         
	        //empty ‚Äö√Ñ√∂‚àö√ë‚àö√Ü Determine whether a variable is empty
	        /*http://php.net/manual/en/function.empty.php*/
	        if(empty($paged) || $paged == 0) {
	            $paged = 1;
	        }
	         
	        $pages_to_show = intval($pagenavi_options['num_pages']);
	        $larger_page_to_show = intval($pagenavi_options['num_larger_page_numbers']);
	        $larger_page_multiple = intval($pagenavi_options['larger_page_numbers_multiple']);
	        $pages_to_show_minus_1 = $pages_to_show - 1;
	        $half_page_start = floor($pages_to_show_minus_1/2);
	        //ceil ‚Äö√Ñ√∂‚àö√ë‚àö√Ü Round fractions up (http://us2.php.net/manual/en/function.ceil.php)
	        $half_page_end = ceil($pages_to_show_minus_1/2);
	        $start_page = $paged - $half_page_start;
	         
	        if($start_page <= 0) {
	            $start_page = 1;
	        }
	         
	        $end_page = $paged + $half_page_end;
	        if(($end_page - $start_page) != $pages_to_show_minus_1) {
	            $end_page = $start_page + $pages_to_show_minus_1;
	        }
	        if($end_page > $max_page) {
	            $start_page = $max_page - $pages_to_show_minus_1;
	            $end_page = $max_page;
	        }
	        if($start_page <= 0) {
	            $start_page = 1;
	        }
	         
	        $larger_per_page = $larger_page_to_show*$larger_page_multiple;
	        //round_num() custom function - Rounds To The Nearest Value.
	        $larger_start_page_start = (round_num($start_page, 10) + $larger_page_multiple) - $larger_per_page;
	        $larger_start_page_end = round_num($start_page, 10) + $larger_page_multiple;
	        $larger_end_page_start = round_num($end_page, 10) + $larger_page_multiple;
	        $larger_end_page_end = round_num($end_page, 10) + ($larger_per_page);
	         
	        if($larger_start_page_end - $larger_page_multiple == $start_page) {
	            $larger_start_page_start = $larger_start_page_start - $larger_page_multiple;
	            $larger_start_page_end = $larger_start_page_end - $larger_page_multiple;
	        }  
	        if($larger_start_page_start <= 0) {
	            $larger_start_page_start = $larger_page_multiple;
	        }
	        if($larger_start_page_end > $max_page) {
	            $larger_start_page_end = $max_page;
	        }
	        if($larger_end_page_end > $max_page) {
	            $larger_end_page_end = $max_page;
	        }
	        if($max_page > 1 || intval($pagenavi_options['always_show']) == 1) {
	            /*http://php.net/manual/en/function.str-replace.php */
	            /*number_format_i18n(): Converts integer number to format based on locale (wp-includes/functions.php*/
	            $pages_text = str_replace("%CURRENT_PAGE%", number_format_i18n($paged), $pagenavi_options['pages_text']);
	            $pages_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pages_text);
	            echo $before.'<div class="pagenavi">'."\n";
	             
	            if(!empty($pages_text)) {
	                echo '<span class="pages">'.$pages_text.'</span>';
	            }
	            //Displays a link to the previous post which exists in chronological order from the current post.
	            /*http://codex.wordpress.org/Function_Reference/previous_post_link*/
	            previous_posts_link($pagenavi_options['prev_text']);
	             
	            if ($start_page >= 2 && $pages_to_show < $max_page) {
	                $first_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['first_text']);
	                //esc_url(): Encodes < > & " ' (less than, greater than, ampersand, double quote, single quote).
	                /*http://codex.wordpress.org/Data_Validation*/
	                //get_pagenum_link():(wp-includes/link-template.php)-Retrieve get links for page numbers.
	                echo '<a href="'.esc_url(get_pagenum_link()).'" class="first" title="'.$first_page_text.'">1</a>';
	                if(!empty($pagenavi_options['dotleft_text'])) {
	                    echo '<span class="expand">'.$pagenavi_options['dotleft_text'].'</span>';
	                }
	            }
	             
	            if($larger_page_to_show > 0 && $larger_start_page_start > 0 && $larger_start_page_end <= $max_page) {
	                for($i = $larger_start_page_start; $i < $larger_start_page_end; $i+=$larger_page_multiple) {
	                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
	                    echo '<a href="'.esc_url(get_pagenum_link($i)).'" class="single_page" title="'.$page_text.'">'.$page_text.'</a>';
	                }
	            }
	             
	            for($i = $start_page; $i  <= $end_page; $i++) {                     
	                if($i == $paged) {
	                    $current_page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['current_text']);
	                    echo '<span class="current">'.$current_page_text.'</span>';
	                } else {
	                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
	                    echo '<a href="'.esc_url(get_pagenum_link($i)).'" class="single_page" title="'.$page_text.'">'.$page_text.'</a>';
	                }
	            }
	             
	            if ($end_page < $max_page) {
	                if(!empty($pagenavi_options['dotright_text'])) {
	                    echo '<span class="expand">'.$pagenavi_options['dotright_text'].'</span>';
	                }
	                $last_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['last_text']);
	                echo '<a href="'.esc_url(get_pagenum_link($max_page)).'" class="last" title="'.$last_page_text.'">'.$max_page.'</a>';
	            }
	            next_posts_link($pagenavi_options['next_text'], $max_page);
	             
	            if($larger_page_to_show > 0 && $larger_end_page_start < $max_page) {
	                for($i = $larger_end_page_start; $i <= $larger_end_page_end; $i+=$larger_page_multiple) {
	                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
	                    echo '<a href="'.esc_url(get_pagenum_link($i)).'" class="single_page" title="'.$page_text.'">'.$page_text.'</a>';
	                }
	            }
	            echo '</div>'.$after."\n";
	        }
	    }
	}

/* Shortcode para Comentarios */

	function mostrar_comentarios_function() {

		global $wpdb;
		 
		$sql = "SELECT * FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' AND post_type='post' ORDER BY comment_date_gmt DESC LIMIT 50";
		 
		$comments = $wpdb->get_results($sql);

		//print_r($comments);
		 
		foreach ($comments as $comment) {
			//print_r($comment);
			    // the permalink for the comment
			  $link = get_permalink($comment->ID) . '#comment-' . $comment->comment_ID;
			 
			  // the post title where that comment was posted
			  echo '<div class="comment-producto"><a href="'.$link.'">'.$comment->post_title.'</a></div>'; 

		  // we need the comment comment user_id in case it's a logged in user so we can echo the display name and not username
			  if ($comment->user_id != 0) {
			    $curent_userdata = get_userdata($comment->user_id);
			    echo '<div class="comment-author">'.$curent_userdata->display_name.'el '.$comment->comment_date.'</div>';
			  } else {
			    echo '<div class="comment-author">'.strip_tags($comment->comment_author).'el '.$comment->comment_date.'</div>';
			  }

			 
			  // display the comment content
			  echo strip_tags($comment->comment_content);
		}
	}
	
	add_shortcode('comentarios', 'mostrar_comentarios_function');
}

function rss_post_thumbnail($content) {
	global $post;
	if(has_post_thumbnail($post->ID)) {
	$content = '<p>' . get_the_post_thumbnail($post->ID, medium) .
	'</p>' . get_the_content();
	}
	return $content;
}

add_filter('the_excerpt_rss', 'rss_post_thumbnail');
add_filter('the_content_feed', 'rss_post_thumbnail');
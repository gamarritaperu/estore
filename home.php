<?php get_header(); ?>

<?php if (get_option('estore_scroller') == 'on') get_template_part('includes/scroller'); ?>

	<div id="banner-publicitarios">
		<a href="http://www.decoracionestito.com/"><img src="http://b27a353aad5c4c1423b0-3e5d5b32ec9effd169e6ecfe78e4516d.r97.cf1.rackcdn.com/wp-content/uploads/2013/05/banner-decoraciones-tito.jpg" alt="Banner Decoraciones Tito" height="125" weight="125" /></a>
		<a href="http://gamarritaperu.com/tienda/plenum"><img src="http://b27a353aad5c4c1423b0-3e5d5b32ec9effd169e6ecfe78e4516d.r97.cf1.rackcdn.com/wp-content/uploads/2013/04/banner-plenum.jpg" alt="Banner tienda Plenum" height="125" weight="125" /></a>
		<a href="http://boston.com.pe"><img src="http://b27a353aad5c4c1423b0-3e5d5b32ec9effd169e6ecfe78e4516d.r97.cf1.rackcdn.com/wp-content/uploads/2013/05/banner-boston.png" alt="Banner Boston" height="125" weight="125" /></a>
		<a href="http://www.gamarritaperu.com/tienda/decoraciones-paolo/"><img src="http://b27a353aad5c4c1423b0-3e5d5b32ec9effd169e6ecfe78e4516d.r97.cf1.rackcdn.com/wp-content/uploads/2013/05/banner-decoraciones-paolo.png" alt="Banner Decoraciones Paolo" height="125" weight="125" /></a>
		<a href="http://gamarritaperu.com/galeria/galeria-la-41-de-gamarra/"><img src="http://b27a353aad5c4c1423b0-3e5d5b32ec9effd169e6ecfe78e4516d.r97.cf1.rackcdn.com/wp-content/uploads/2013/09/banner-la-once.jpg" alt="Banner La Once 41" height="120" weight="172" /></a>
	</div>

<div id="slider-portada">
	<?php echo do_shortcode("[metaslider id=12233]"); ?>
	<?php echo do_shortcode("[metaslider id=12234]"); ?>
	<div id="mailchimp">
		<h4>Recibe ofertas por e-mail</h4>
		<?php echo do_shortcode("[mc4wp-form]"); ?>
	</div>
</div>

<div id="main-area">
	<div id="main-content" class="clearfix">
		<div id="left-column">
			<?php get_template_part('includes/entry'); ?>
		</div> <!-- #left-column -->

		<?php get_sidebar(); ?>

<?php get_footer(); ?>

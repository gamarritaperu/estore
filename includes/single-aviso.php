<?php $custom = get_post_custom( get_the_ID() );
$et_price = isset($custom["price"][0]) ? $custom["price"][0] : '';
if ($et_price <> '') $et_price = get_option('estore_cur_sign') . $et_price;
$et_description = isset($custom["description"][0]) ? $custom["description"][0] : '';
$et_band =  isset($custom["et_band"][0]) ? 'et_' . $custom["et_band"][0] : '';

$custom["thumbs"] = isset( $custom["thumbs"][0] ) ? unserialize($custom["thumbs"][0]) : '';

$thumb = '';
$width = 298;
$height = 130;
$classtext = '';
$titletext = get_the_title();

$thumbnail = get_thumbnail($width,$height,$classtext,$titletext,$titletext,true);
$thumb = $thumbnail["thumb"]; ?>

<?php if ($et_band <> '') { ?>
	<span class="band<?php echo(' '.esc_attr($et_band)); ?>"></span>
<?php }; ?>

<h1 class="title"><?php the_title(); ?></h1>

<div class="clear"></div>

<?php the_content(); ?>
<div class="clear"></div>

<div class="hr"></div>
<?php comments_template(); ?>

<?php wp_link_pages(array('before' => '<p><strong>'.esc_html__('Pages','eStore').':</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
<?php edit_post_link(esc_html__('Edit this page','eStore')); ?>


<?php $orig_post = $post;
global $post;
$tags = wp_get_post_tags( $post->ID );
if ($tags) {
	$tag_ids = array();

	foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
	$args=array(
		'tag__in' => $tag_ids,
		'post__not_in' => array( $post->ID ),
		'posts_per_page'=>4,
		'ignore_sticky_posts'=>1,
	);
	$my_query = new wp_query( $args );

	if( $my_query->have_posts() ) { ?>
		<div class="related">
			<h2><?php esc_html_e('Related Products','eStore'); ?></h2>
			<ul class="related-items clearfix">
				<?php $i=1; while( $my_query->have_posts() ) {
				$my_query->the_post(); ?>
					<?php $thumb = '';
					$width = 44;
					$height = 44;
					$classtext = '';
					$titletext = get_the_title();

					$thumbnail = get_thumbnail($width,$height,$classtext,$titletext,$titletext);
					$thumb = $thumbnail["thumb"]; ?>

					<li<?php if($i%2==0) echo(' class="second"'); ?>>
						<a href="<?php the_permalink(); ?>" class="clearfix">
							<?php if ($thumb <> '') print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height, $classtext); ?>
							<span><?php the_title(); ?></span>
						</a>
					</li>
					<?php $i++; ?>
				<?php } ?>
			</ul>
		</div>
	<?php }
}
$post = $orig_post;
wp_reset_query(); ?>